;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(in-package #:gtk)

(defenum gtkreliefstyle
  (:gtk-relief-normal
   :gtk-relief-half
   :gtk-relief-none))

(defenum gtkwindowtype
  (:gtk-window-toplevel
   :gtk-window-popup))

(defenum gsignalflags
  ((:g-signal-none        .  0)
   (:g-signal-run-first   .  1)
   (:g-signal-run-last    .  2)
   (:g-signal-run-cleanup .  4)
   (:g-signal-no-recurse  .  8)
   (:g-signal-detailed    . 16)
   (:g-signal-action      . 32)
   (:g-signal-no-hooks    . 64))

  :bitwise t)


(defenum gconnectflags
  ((:g-connect-none     . 0)
   (:g-connect-after    . 1)
   (:g-connect-swapped  . 2))

  :bitwise t)

(defenum gtkattachoptions
  ((:gtk-expand . 1)
   (:gtk-shrink . 2)
   (:gtk-fill   . 4))

  :bitwise t)

(defenum gtkpositiontype
  (:gtk-pos-left
   :gtk-pos-right
   :gtk-pos-top
   :gtk-pos-bottom))

(defenum gtkjustification
  (:gtk-justify-left
   :gtk-justify-right
   :gtk-justify-center
   :gtk-justify-fill))

(defenum gtkarrowtype
  (:gtk-arrow-up
   :gtk-arrow-down
   :gtk-arrow-left
   :gtk-arrow-right))

(defenum gtkshadowtype
  (:gtk-shadow-none
   :gtk-shadow-in
   :gtk-shadow-out
   :gtk-shadow-etched-in
   :gtk-shadow-etched-out))

