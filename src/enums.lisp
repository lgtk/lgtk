;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;;; An FFI enhancement for C enums
(defpackage #:enums
  (:export #:defenum #:translate)
  (:use :common-lisp :defbinding))

(in-package #:enums)

;; A package where we store enum information
(defpackage #:enum-land)

(defstruct edata
  alist strict bitwise mask)

;; Take the enum definition list and convert it to an alist 
(defun alistize (elist)
  (labels ((errorexit (s)
		      (error (format nil "~S is an invalid enum ~
                                         clause. See documentation ~
                                         for details." s)))

	   (validate-and-frob (item idx)
			      (cond ((keywordp item) (cons item idx))

				    ((and (consp item)
					  (keywordp (car item)))
				     (let ((rv (eval (cdr item))))
				       (if (typep rv 'fixnum)
					   (cons (car item) rv)
					 (errorexit item))))

				    (t (errorexit item)))))

    (let (acc)
      (do ((idx 0 (1+ idx))
	   (shtl elist (cdr shtl)))
	  ((null shtl) acc)

	(push (validate-and-frob (car shtl) idx)
	      acc)

	(setf idx (cdar acc))))))


;; Given a key, return the value, or given a value, return it. Also
;; validates if the value is of the right type.
(defun translate (symbol val)
  (let ((res (cond ((keywordp val)
		    (cdr (assoc val (edata-alist (get symbol 'enum)))))
		   ((typep val symbol) val)
		   (t nil))))
    (if (not res)
	(error (format nil "~A is not of enum type ~A." val symbol))
      res)))

;; Convert a form like (:optor :opt1 :opt2 val ..etc into a lambda
;; expression which evaluates well.
(defmacro translated-form (edata dfunc symbol ops)
  (let (args
	pendings
	(alist (edata-alist edata)))

    (labels ((delay (x)
		    (let ((v (gensym)))
		      (push v args)
		      (push `(translate ',symbol ,x) pendings)
		      v))

	     (analyze (item)
		       (cond ((keywordp item) (cdr (assoc item alist)))
			     ((consp item)
			      (let* ((op (car item))
				     (rest (cdr item))
				     (rop (cdr (assoc op ops))))
			      
				(if rop
				    (cons rop (mapcar #'analyze rest))
				  (delay item))))
			     (t (delay item)))))

      (let ((code (analyze dfunc)))
	`((lambda ,(reverse args)
	    ,code) ,@(reverse pendings))))))

(defmacro defenum (name stuff
			&key
			(package (package-name *package*))
			(strict t)
			(bitwise nil))

  (let* ((symb (intern (format nil "~A{ enum }~A" package name) :enum-land))
	 (ast (alistize stuff))
	 (msk (if bitwise (reduce #'logior ast :key #'cdr)))
	 (arg (gensym))
	 (predicate-name (intern (format nil "~A-p" symb) :enum-land))
	 tconds)

    (if (and strict (not bitwise))
	(push `(lambda (,arg)
		(rassoc ,arg (edata-alist ,symb)))
	      tconds))

    (if (and bitwise strict)
	(push `(lambda (,arg)
		(= ,msk (logior ,arg ,msk)))
	      tconds))

    ;; always
    (push `(lambda (,arg)
	    (assoc ,arg (edata-alist ,symb)))
	  tconds)

    (if (not strict)
	(push 'fixnump tconds))

    `(eval-when (:compile-toplevel :load-toplevel :execute)
      (defparameter ,symb
	(make-edata :alist (quote ,ast)
		    :strict ,strict
		    :bitwise ,bitwise
		    :mask ,msk))

      (setf (get ',name 'enum) ,symb)

      (defun ,predicate-name (,arg)
	(or ,@(mapcar (lambda (pred)
			`(,pred ,arg))
		      tconds)))
      
      (deftype ,name ()
	'(satisfies ,predicate-name))

      (defmacro ,name (,arg)
	`(translated-form ,,symb ,,arg ,',name
	  ,',(if bitwise '((:optor . logior)
			   (:optand . logand)))))

      (def-binding-type ,name
	  :in ',name
	  :alien :int))))
