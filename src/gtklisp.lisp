;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(in-package :gtk)

(def-callback-type gtkdelete-event
  "delete_event" gtk-evhandling-decoy)

(def-callback-type gtkdestroy
  "destroy" gtk-destroy-decoy)

(def-callback-type gtkclicked
  "clicked" gtk-standard-decoy)

(def-callback-type gtkvalue-changed
  "value_changed" gtk-standard-decoy)

(def-callback-type gtktoggled
  "toggled" gtk-standard-decoy)

(defun g-signal-connect (wid signal func &key data (flags :g-connect-none))
;; the data argument is a keyword arg because we do not need to fake
;; closures.
  (declare (optimize (debug 3)))
  (let* ((rw (contents wid))
	 (it (make-instance 'gtk-object-callback
			    :object wid
			    :func func
			    :data data
			    :nexus *gtkcallbacks*))
	 (meta (meta wid)))

    (setf (contents it)
	  (gtk-aliens::|g_signal_connect_data|
	   rw (marker signal) (trampoline signal) (id it)
	   nil (gconnectflags flags)))

    (if (eql signal gtkdestroy)
	(pushnew (meta it) (destroyers meta))
      (pushnew (meta it) (callbacks meta)))

    (gcconnect it wid)

    (debugf t "Tacked callback id ~A to widget ~a for ~s.~%"
	    (id it) wid (marker signal))))

(defun glade-xml-signal-connect (wid signal-name func
				     &key data)
;; the data argument is a keyword arg because we do not need to fake
;; closures.
  (declare (optimize (debug 3)))
  (let* ((rw (contents wid))
	 (it (make-instance 'gtk-object-callback
			    :object wid
			    :func func
			    :data data
			    :nexus *gtkcallbacks*))
	 (meta (meta wid)))

    (setf (contents it)
	  (gtk-aliens::|glade_xml_signal_connect_data|
	   rw signal-name (c-fun-ptr gtk-standard-decoy) (id it)))

    '(if (eql signal gtkdestroy)
	(pushnew (meta it) (destroyers meta))
      (pushnew (meta it) (callbacks meta)))

    (gcconnect it wid)

    (debugf t "Tacked (glade) callback id ~A to widget ~a for ~s.~%"
	    (id it) wid signal-name)))

(defun g-signal-connect-swapped
  (wid signal func &key data (flags :g-connect-none))

  (g-signal-connect wid signal

		    #'(lambda (&rest args)
			(declare (ignore args))
			(funcall func data))

		    :data nil
		    :flags flags))

(defun gtk-timeout-add (msecs func &key data)
  (let ((it (make-instance 'gtk-timeout-callback
			   :nexus *gtkcallbacks*
			   :data data
			   :func func)))
    (setf (contents it)
	  (gtk-aliens::|gtk_timeout_add| msecs
				 (c-fun-ptr %gtk-itc-handler)
				(id it)))

    it))

(defun gtk-timeout-remove (it)
  (destroy (meta it)))

(defun gtk-idle-add (func &key data)
  (let ((it (make-instance 'gtk-idle-callback
			   :nexus *gtkcallbacks*
			   :data data
			   :func func)))
    (setf (contents it)
	  (gtk-aliens::|gtk_idle_add| (c-fun-ptr %gtk-itc-handler)
			      (id it)))

    it))

(defun gtk-idle-remove (it)
  (destroy (meta it)))

(defun gtkocapsule (realw)
  (let ((addrnum (alien-address realw)))
    (if (zerop addrnum) nil
      (let ((isit (gethash (alien-address realw)
			   (table *gtkobjects*))))
	(if isit (capsule isit)
	  (let ((it (make-instance 'gtk-objcapsule
				   :contents realw
				   :nexus *gtkobjects*)))

	    ;; We need at least one destroy call to know it's over and remove
	    ;; all activable trace of the widget.
	    (g-signal-connect it gtkdestroy
			      #'dummy-func)

	    it))))))

(defun alien-encapsulate (realw capsule &key (destroy t))
  (let ((addrnum (alien-address realw)))
    (if (zerop addrnum) nil
      (let ((isit (gethash (alien-address realw)
			   (table *gtkobjects*))))
	(if isit (capsule isit)
	  (let ((it (make-instance capsule
				   :contents realw
				   :nexus *gtkobjects*
				   :destroy-real-object destroy)))
	    it))))))

;; We need at least one destroy call to know it's over and remove
;; all activable trace of the widget.
(defmethod initialize-instance :after ((it gtk-objcapsule) &key)
  (g-signal-connect it
		    gtkdestroy
		    #'dummy-func))

(defmethod initialize-instance :after ((it g-objcapsule) &key)
  (g-signal-connect it
		    gtkdestroy    ;; it should not be called like this.
		    #'dummy-func))



;; Initialize.
(eval-when (:load-toplevel :execute :compile-toplevel)

  (defvar *gtk-init* nil)

  (defun gtk-init ()
    (when (not *gtk-init*)
      (let ((i 0))
	(gtk-aliens::|gtk_init| i 
		     (voidptr 0)))
      (setf *gtk-init* t)))

  (gtk-init))

;; The armored main loop kludge.
(defun gtk-main ()
  (if *main-active*
      (break "GTK Main-loop is already active." t))

  (let ((timeout)
	(*main-active* t))

    (unwind-protect
	(labels ((my-handler (a b c)
		     (if *in-main* ; The signal was caught
					; in the middle of C land.

			 ;; If so, schedule handler in 1msec (but then
			 ;; it will run safely in Lisp-land).
			 (setf timeout
			       (gtk-timeout-add
				1 #'(lambda (arg) (declare (ignore arg))
				      (gtk-timeout-remove timeout)
				      (funcall *sigint-handler*
					       a b c))))

		       ;; In case interruption happens in Lisp land
		       (progn
			 (debugf t "Calling old handler.~%")
			 (funcall *sigint-handler* a b c)))))

	  (setf *sigint-handler*
		(swap-unix-sigint-handler #'my-handler))
	  
	  (let ((*in-main* t))
	    (gtk-aliens::|gtk_main|))

	  ;; The user wants back to the top-level
	  (when *tunnel*
	    (setf *tunnel* nil)
	    (debugf t "Tunnel successfull.~%")
	    ;; Throw him to the top-level.
	    (throw 'common-lisp::top-level-catcher nil)))

      ;; When unwinding
      (swap-unix-sigint-handler *sigint-handler*)
      (setf *sigint-handler* nil))))

;; So far, so good.

(defun live-for-1msec ()
    (labels ((didit (data) (declare (ignore data))
		    (gtk-main-quit)
		    0))

      (gtk-timeout-add 1 #'didit)

      (gtk-main)))

(defvar *visual-gc-scheduled* nil)

(defun schedule-visual-gc ()
  (when (not (or *visual-gc-scheduled* *main-active*))
    (setf *visual-gc-scheduled* t)
    #+cmu
    (mp:make-process
     #'(lambda ()
	 (sleep 1)
	 (unless *main-active*
	   (live-for-1msec))
	 (setf *visual-gc-scheduled* nil)))
    ;; Need to provide SBCL alternative here. -dd
    #+sbcl 
    t))

(eval-when (:load-toplevel)
  (run-after-gc #'schedule-visual-gc))

;; A debugging aid.
(defun shash ()
  (format t "~%CALLBACKS:~%")
  (maphash #'(lambda (k x)
	       (format t "k:~S o:~S~%" k x))
	   (NEXUS:TABLE *gtkcallbacks*))
  (format t "~%WIDGETS:~%")
  (maphash #'(lambda (k x)
	       (format t "k:~S o:~S~%" k x))
	   (NEXUS:TABLE *gtkobjects*)))
