;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) November 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(defpackage #:dynaslot
  (:export #:begin-slot-declarations #:generate-alien-accessors
	   #:add-alien-slots #:peek #:poke)
  (:use :defbinding :nexus :common-lisp :clnexus-port))

(in-package :dynaslot)

(defparameter *slot-queue* 'blip)

(defun generate-includes (headers stream)
  (dolist (x headers)
    (format stream "#include <~A>~%" x))
  (format stream "#include <stdio.h>~%~%"))

(defun generate-sub-probe (stname fields stream)
  (format stream "{~A _d_e_c_o_y_;~%" stname)
  (format stream "printf(\"(\");~%")
  (dolist (x fields)
    (format stream
	    "printf(\"%d \",((int)&_d_e_c_o_y_.~A)-((int)&_d_e_c_o_y_));~%"
	    (car x)))
  (format stream "printf(\")\\n\");}~%"))

(defun generate-probe (headers slotqueue fname)
  (with-open-file (stream fname
			  :direction :output
			  :if-exists :supersede)
		  (generate-includes headers stream)
		  (format stream "~%~%int main() {~%")
		  (format stream "printf(\"(\");~%")
		  (dolist (sdata slotqueue)
		    (generate-sub-probe (cadr sdata) (caddr sdata) stream))
		  (format stream "printf(\")\\n\");~%")
		  (format stream "return 0;~%}~%")))

(defmacro begin-slot-declarations ()
  '(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf *slot-queue* nil)))

(defmacro add-alien-slots (class cname structlist)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (push '(,class ,cname ,structlist)
	   *slot-queue*)
     nil))

(defun generate-them-accessors (oname reqlist offslist)
  (labels ((gen1 (req offs)
		 (destructuring-bind
		     (name
		      type
		      &key
		      (reader t)
		      (writer t)
		      (export t)
		      (destroy nil)
		      (lisp-name nil))
		     req
		   (let ((sname (if lisp-name lisp-name
				  (intern (format nil "~A-~A" oname
						  (map 'string
						       #'char-upcase
						       (_2- name)))))))
		     `(
			,(if reader 
			     `(defmethod ,sname ((x ,oname))
				,(buildform (out-filter type :destroy destroy)
					    `(peek (contents x) ,offs
						   ,(alien-type type)))))
			,(if writer
			     `(defmethod (setf ,sname) (value (x ,oname))
				(poke (contents x) ,offs
				      ,(alien-type type)
				      ,(buildform (in-filter type)
						  'value))))
			,(if export `(export ',sname)))))))
    (reduce #'append (mapcar #'gen1 reqlist offslist))))

(defmacro generate-alien-accessors (&key cflags cc headers probedir)
  (let ((cflags (eval cflags))
	(cc (eval cc))
	(headers (eval headers))
	(probedir (eval probedir)))

    (let ((src (make-pathname :name "probe"
			      :type "c"
			      :directory probedir))
	  (exe (make-pathname :name "probe"
			      :type nil
			      :directory probedir))
	  (out (make-pathname :name "probe"
			      :type "x"
			      :directory probedir)))
      (generate-probe headers *slot-queue* src)
      (when (asdf:run-shell-command "~A ~A -o ~A ~A"
				    cc
				    (namestring src)
				    (namestring exe)
				    cflags)
	(asdf:run-shell-command "~A > ~A"
				(namestring exe)
				(namestring out))
	(let ((offsl (with-open-file (strm out :direction :input)
				     (read strm)))
	      (reqs *slot-queue*))
	  
	  `(eval-when (:compile-toplevel :load-toplevel :execute)
	     ,@(reduce #'append
		       (mapcar #'(lambda (off req)
				   (generate-them-accessors
				    (car req) (caddr req) off))
			       offsl reqs))))))))

;; This is how this should be used.
#||
(begin-slot-declarations)

(add-alien-slots gtkdialog "GtkDialog"
		  (("window" gtkwindow)
		   ("vbox" gtkvbox)
		   ("action_area" gtkhbox)))

(add-alien-slots gtkadjustment "GtkAdjustment"
		 (("lower" gtk::gdouble)
		  ("upper" gtk::gdouble)))

(generate-alien-accessors
 :cflags lgtk-asd::*gtkcflags*
 :cc lgtk-asd::*ccompiler*
 :headers '("gtk/gtk.h")
 :probedir lgtk-asd::*source-dir*)

||#

