/*

 (c) May 2004 by Mario S. Mommer <mommer@well-of-wyrd.net>

 This code comes under the terms of the modified BSD license ("sans
 advertising clause"). See the file COPYING for details.

*/

#include <gtk/gtkobject.h>

unsigned int lgtk_object_type (void *object) {
	return GTK_OBJECT_TYPE (object);
}

void dummy_function () {}
