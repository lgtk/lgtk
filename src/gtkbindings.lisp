;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(in-package :gtk)

;; Callback and events
(defun gfloat-coerce (x)
  (coerce x 'single-float))

(defun gdouble-coerce (x)
  (coerce x 'double-float))

(defun rbooltrans (x)
  (if (/= x 0) t nil))

(defclass gslist (sapcapsule) ())

(def-encapsulator gslist-encap gslist)

(defclass GladeXML (sapcapsule) ())

(def-encapsulator GladeXML-encap GladeXML)

;; For types where nil is acceptable as an object
(defun contents-nil (obj)
  (cond ((null obj) obj)
	(t (contents obj))))

(def-bindings-types

;  (gtkobject
;   :in 'contents-nil
;   :out 'gtkocapsule
;   :alien '(* t))

  (gslist
   :in 'contents-nil
   :out 'gslist-encap
   :alien '(* t))

  (GladeXML
   :in 'contents-nil
   :out 'GladeXML-encap
   :alien '(* t))

;  (gtkwindowtype
;   :in 'gtkwindowtype
;   :alien :int)

;  (gtkattachoptions
;   :in 'gtkattachoptions
;   :alien :int)

;  (gtkpositiontype
;   :in 'gtkpositiontype
;   :alien :int)

  (c-string
   :alien :c-string)

  (void
   :alien :void)

  (guint
   :alien :uint)

  (gint
   :alien :int)

  (gulong
   :alien :ulong)

  (gfloat
   :in 'gfloat-coerce
   :alien :float)

  (gdouble
   :in 'gdouble-coerce
   :alien :double)

  (gboolean
   :in 'booltrans
   :out 'rbooltrans
   :alien :int)

  (gfunc
   :in 'contents
   :alien '(* t))

  ;; Only used for raw bindings
  (voidptr 
   :alien '(* t))

  ;; Lisp types.
  (pathname
   :in 'namestring
   :out 'parse-namestring
   :alien :c-string))

(set-aliens-package "GTK-ALIENS")

(def-binding "gtk_window_new"
  (gtkwindow (gtkwindowtype type)))

(def-binding "gtk_label_new"
  (gtklabel (c-string i)))

(def-binding "gtk_label_new_with_mnemonic"
  (gtklabel (c-string i)))

(def-binding "gtk_label_set_text"
  (void (gtklabel label)
	(c-string i)))

(def-binding "gtk_label_get_text"
  (c-string (gtklabel label)))

(def-binding "gtk_label_set_justify"
  (void (gtklabel l)
	(gtkjustification j)))

(def-binding "gtk_label_set_line_wrap"
  (void (gtklabel label)
	(gboolean  wrap)))

(def-binding "gtk_button_new"
  (gtkbutton))

(def-binding "gtk_button_new_with_label"
  (gtkbutton (c-string text)))

(def-binding "gtk_button_new_with_mnemonic"
  (gtkbutton (c-string text)))


(def-binding "gtk_button_new_from_stock"
  (gtkbutton (c-string text)))

(def-binding "gtk_button_set_relief"
  (void (gtkbutton button)
	(gtkreliefstyle style)))

(def-binding "gtk_button_get_relief"
  (gtkreliefstyle (gtkbutton button)))

(def-binding "gtk_button_get_label"
  (c-string (gtkbutton button)))

(def-binding "gtk_button_set_label"
  (void (gtkbutton button)
	(c-string label)))

(def-binding "gtk_toggle_button_new"
  (gtktogglebutton))

(def-binding "gtk_toggle_button_new_with_label"
  (gtktogglebutton (c-string text)))

(def-binding "gtk_toggle_button_new_with_mnemonic"
  (gtktogglebutton (c-string text)))

(def-binding "gtk_check_button_new"
  (gtkcheckbutton))

(def-binding "gtk_check_button_new_with_label"
  (gtkcheckbutton (c-string text)))

(def-binding "gtk_check_button_new_with_mnemonic"
  (gtkcheckbutton (c-string text)))

(def-binding "gtk_radio_button_new"
  (gtkradiobutton (GSList group)))

(def-binding "gtk_radio_button_new_from_widget"
  (gtkradiobutton (gtkradiobutton button)))

(def-binding "gtk_radio_button_new_with_label"
  (gtkradiobutton (gslist group)
		  (c-string msg)))

(def-binding "gtk_radio_button_new_with_mnemonic"
  (gtkradiobutton (gslist group)
		  (c-string msg)))

(def-binding "gtk_radio_button_new_with_label_from_widget"
  (gtkradiobutton (gtkradiobutton group)
		  (c-string msg)))

(def-binding "gtk_radio_button_new_with_mnemonic_from_widget"
  (gtkradiobutton (gtkradiobutton group)
		  (c-string msg)))

(def-binding "gtk_radio_button_get_group"
  (gslist (gtkradiobutton obj))
  :after (lambda (x) (gcconnect obj x) x))

(def-binding "gtk_toggle_button_get_active"
  (gboolean (gtktogglebutton wid)))

(def-binding "gtk_toggle_button_set_active"
  (void (gtktogglebutton wid)
	(gboolean active)))

(def-binding "gtk_entry_new"
  (gtkentry))

(def-binding "gtk_entry_set_text"
  (void (GtkEntry entry)
	(c-string text)))

(def-binding "gtk_entry_get_text"
  (c-string (GtkEntry entry)))

(def-binding "gtk_editable_set_editable"
  (void (GtkEntry entry)
	(gboolean active)))

(def-binding "gtk_entry_set_visibility"
  (void (GtkEntry entry)
	(gboolean visible)))

(def-binding "gtk_arrow_new"
  (gtkarrow (GtkArrowType arrow_type)
	    (GtkShadowType  shadow_type)))

(def-binding "gtk_arrow_set"
  (void (GtkArrow arrow)
	(GtkArrowType arrow_type)
	(GtkShadowType  shadow_type )))

(def-binding "gtk_tooltips_new"
  (GtkTooltips))

(def-binding "gtk_tooltips_set_tip"
  (void (GtkTooltips tooltips)
	(GtkWidget widget)
	(c-string tip_text)
	(c-string tip_private)))

(def-binding "gtk_dialog_new" (gtkdialog))

(def-binding "gtk_frame_new" (gtkframe (c-string label)))

(def-binding "gtk_table_new"
  (gtktable (guint rows)
	    (guint columns)
	    &key
	    (gboolean homogeneous)))

(def-binding "gtk_hbox_new"
  (gtkhbox &key
	   (gboolean homogeneous)
	   (gint  spacing 0)))

(def-binding "gtk_vbox_new"
  (gtkvbox &key
	   (gboolean homogeneous)
	   (gint  spacing 0)))

(def-binding "gtk_image_new"
  (gtkimage))

(def-binding "gtk_image_new_from_file"
  (gtkimage (pathname name)))

(def-binding "gtk_hseparator_new"
  (gtkhseparator))

(def-binding "gtk_widget_show"
  (void (gtkwidget w)))

(def-binding "gtk_widget_show_all"
  (void (gtkwidget w)))

(def-binding "gtk_widget_set_size_request"
  (void (gtkwidget w)
	(gint width)
	(gint height)))

(def-binding "gtk_container_add"
  (void (gtkcontainer cont)
	(gtkwidget new))
  :before (gcconnect cont new))

(def-binding "gtk_box_pack_start"
  (void (gtkbox box)
	(gtkwidget wid)
	&key
	(gboolean expand)
	(gboolean fill)
	(guint  padding 0))
  :before (gcconnect box wid))

(def-binding "gtk_box_pack_end"
  (void (gtkbox box)
	(gtkwidget wid)
	&key
	(gboolean expand)
	(gboolean fill)
	(guint  padding 0))
  :before (gcconnect box wid))

(def-binding "gtk_table_attach"
  (void (gtktable tab)
	(gtkwidget obj)
	(guint l-attach)
	(guint r-attach)
	(guint t-attach)
	(guint b-attach)
	&key
	(gtkattachoptions
		   xoptions (gtkattachoptions
		   (:optor :gtk-fill :gtk-expand)))
	(gtkattachoptions
		   yoptions (gtkattachoptions
		   (:optor :gtk-fill :gtk-expand)))
	(guint  xpadding 0)
	(guint  ypadding 0))
  :before (gcconnect tab obj))

(def-binding "gtk_container_set_border_width"
  (void (gtkcontainer wid)
	(guint width)))

(def-binding "gtk_window_set_title"
  (void (gtkwindow wid)
	(c-string title)))

(def-binding "gtk_main_quit"
  (void))

(def-binding "gtk_widget_destroy"
  (void (gtkwidget wid)))

(def-binding "gtk_object_destroy"
  (void (gtkobject wid)))

(def-binding "gtk_misc_set_alignment"
  (void (gtkmisc misc)
	(gfloat xalign)
	(gfloat yalign)))

(def-binding "gtk_adjustment_new"
  (gtkadjustment (gdouble value)
		 (gdouble lower)
		 (gdouble upper)
		 (gdouble step_increment)
		 (gdouble page_increment)
		 (gdouble page_size)))

(def-binding "gtk_adjustment_get_value"
  (gdouble (gtkadjustment adjustment)))

(def-binding "gtk_adjustment_set_value"
  (void (gtkadjustment adjustment)
	(gdouble value)))

(def-binding "gtk_hscrollbar_new"
  (gtkhscrollbar (gtkadjustment adjustment))
  :after (lambda (x)
	   (gcconnect x adjustment)
	   x))

(def-binding "gtk_vscrollbar_new"
  (gtkvscrollbar (gtkadjustment adjustment))
  :after (lambda (x)
	   (gcconnect x adjustment)
	   x))

(def-binding "gtk_vscale_new"
  (gtkvscale (gtkadjustment adjustment))
  :after (lambda (x)
	   (gcconnect x adjustment)
	   x))

(def-binding "gtk_vscale_new_with_range"
  (gtkvscale (gdouble min)
	     (gdouble max)
	     (gdouble step)))

(def-binding "gtk_hscale_new"
  (gtkhscale (gtkadjustment adjustment))
  :after (lambda (x)
	   (gcconnect x adjustment)
	   x))

(def-binding "gtk_hscale_new_with_range"
  (gtkhscale (gdouble min)
	     (gdouble max)
	     (gdouble step)))

(def-binding "gtk_scale_set_draw_value"
  (void (gtkscale scale)
	(gboolean draw_value)))

(def-binding "gtk_scale_set_digits"
  (void (gtkscale scale)
	(gint     digits)))

(def-binding "gtk_scale_set_value_pos" 
  (void (gtkscale scale)
	(GtkPositionType  pos)))

(def-binding "gtk_range_get_adjustment"
  (gtkadjustment (gtkrange range))
  :after (lambda (x)
	   (gcconnect x range)
	   x))

(begin-slot-declarations)

(add-alien-slots gtkdialog "GtkDialog"
		  (("window" gtkwindow)
		   ("vbox" gtkvbox)
		   ("action_area" gtkhbox)))


(generate-alien-accessors
 :cflags lgtk-asd::*gtkcflags*
 :cc lgtk-asd::*ccompiler*
 :headers '("gtk/gtk.h")
 :probedir lgtk-asd::*source-dir*)


;; Raw bindings. These need special care
(def-raw-binding "g_signal_connect_data"
  (gulong (gtkobject wid)
	  (c-string name)
	  (gfunc func)
	  (guint data)
	  (gfunc closurenotify)
	  (gint flags)))

(def-raw-binding "glade_xml_signal_connect_data"
  (void (GladeXML xml)
	(c-string name)
	(gfunc fun)
	(guint data)))

(def-raw-binding "gtk_main" (void))

(def-raw-binding "gtk_init"
  (void ((gint) i)
	(voidptr v)))

(def-raw-binding "g_signal_handler_disconnect"
  (void (voidptr instance)
	(guint id)))

(def-raw-binding "g_object_unref"
  (void (voidptr unref)))

(def-raw-binding "gtk_timeout_add"
  (guint (guint interval)
	 (gfunc function)
	 (guint data)))

(def-raw-binding "gtk_timeout_remove"
  (void (guint id)))

(def-raw-binding "gtk_idle_add"
  (guint (gfunc function)
	 (guint data)))

(def-raw-binding "gtk_idle_remove"
  (void (guint id)))
