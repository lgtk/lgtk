;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;; Facilities for making bindings. Essentially an FFI interface.
(defpackage #:defbinding
  (:export #:def-binding #:def-bindings-types #:def-raw-binding
	   #:set-aliens-package #:def-binding-type
	   #:in-filter #:out-filter #:alien-type #:buildform #:_2-)
  (:use :common-lisp :clnexus-port))

(in-package #:defbinding)

(defun _2- (name)
  (let ((cp (copy-seq name)))
    (dotimes (i (length cp) cp)
      (when (equal (aref cp i) #\_)
	(setf (aref cp i) #\-)))))

(defstruct binding-type
  in out alien)

(defmacro def-binding-type (name &rest stuff)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf (get ',name 'binding-type)
	   (make-binding-type ,@stuff))))

(defmacro def-bindings-types (&rest stuff)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ,@(mapcar #'(lambda (expr)
		   `(def-binding-type ,@expr))
	       stuff)))

(defmacro set-aliens-package (name)
  (let ((glob (intern "*ALIENS-PACKAGE*")))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (defparameter ,glob ,name)
       (defpackage ,name))))

(defun aliens-package ()
  (eval (find-symbol "*ALIENS-PACKAGE*")))

(defun alien-type (symbol)
  (port-alien-type (binding-type-alien (get symbol 'binding-type))))

(defun in-filter (symbol &rest args)
  (let ((bto (binding-type-in (get symbol 'binding-type)))
	(x (gensym "out-filter")))
    (if bto
	(if args
	    `(lambda (,x)
	       (,bto ,x ,@args))
	  bto)
      (if args
	  (error "filter NIL does not accept parameters (obviously).")))))

(defun out-filter (symbol &rest args)
  (let ((bto (binding-type-out (get symbol 'binding-type)))
	(x (gensym "out-filter")))
    (if bto
	(if args
	    `(lambda (,x)
	       (,bto ,x ,@args))
	  bto)
      (if args
	  (error "filter NIL does not accept parameters (obviously).")))))

(defun buildform (func arg)
  (if func (list func arg)
    arg))

;; The back end
(defun aliendecl (symb name rt args)
  `(def-alien-routine (,name ,symb)
     ,(alien-type rt)
     ,@(mapcar #'(lambda (e)
		   (cons (cadr e)
			 (if (consp (car e))
			     (list (alien-type (caar e))
				   :in-out)
			   (cons (alien-type (car e)) nil))))
	       args)))

(defun make-args (args)
  (let (defon)
    (mapcar #'(lambda (x)
		(cond ((and defon (consp x))
		       (cons (cadr x) (cddr x)))
		      ((consp x) (cadr x))
		      (t (setf defon t) x)))
	    args)))

(defmacro def-binding (name typestuff &key before &key after)

  (labels ((set-filters (rt args expr)
	      (let* ((st1 (cons `(,rt)
				(mapcar #'(lambda (x)
					    (car x))
					args)))
		     (st2 (delete-if-not #'consp st1))
		     (vars (mapcar #'(lambda (x)
				       (gensym (format nil "~A"
						       (car x))))
				   st2))

		     (rforms (mapcar #'(lambda (x y)
					(buildform
					 (out-filter (car x)) y))
				     st2 vars)))
		`(multiple-value-bind ,vars
		     ,expr
		   (,(if after after 'values) ,@rforms)))))

    (let* ((asymb (intern name (aliens-package)))
	   (rsymb (intern (string-upcase (_2- name)))))
      (destructuring-bind (rtype . args)
	  typestuff
	(let ((clargs (remove '&key
			      (remove '&optional args))))
	  `(eval-when (:compile-toplevel :load-toplevel :execute)
	     ,(aliendecl asymb name rtype clargs)
	     (defun ,rsymb ,(make-args args)
	       ,before
	       ,(set-filters rtype clargs `(,asymb
				       ,@(mapcar #'(lambda (x)
						     (buildform
						      (in-filter (car x))
						      (cadr x)))
						 clargs))))
	     (export ',rsymb)))))))

(defmacro def-raw-binding (name typestuff)
  (let* ((asymb (intern name (aliens-package))))
    (destructuring-bind (rtype . args)
	typestuff

      `(eval-when (:compile-toplevel :load-toplevel :execute)
	 ,(aliendecl asymb name rtype args)))))
