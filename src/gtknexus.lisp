;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(in-package :gtk)

(defvar *gtkobjects* (make-instance 'nexus))
(defvar *gtkcallbacks* (make-instance 'idnexus))

(defmethod destroy ((m gtk-objmeta))
  (debugf t "Here we go destroying a gtk-objmeta. ID: ~X~%"
	  (metacapsule-identify m))
  (let ((standing (destroyers m)))
    (cond ((and standing (destroy-real-object m) (contents m))
	   (debugf t "It is still standing.~%")
	   (mapcar #'destroy (callbacks m))
	   (debugf t "Callbacks deallocated.~%")
	   (mapcar #'destroy (destroyers m))
	   (debugf t "Destroyers removed.~%")
	   (gtk-aliens::|gtk_object_destroy| (contents m))
	   (debugf t "Object killed.~%")
	   (call-next-method))
	  (t (mapcar #'(lambda (x)
			 (setf (retire-p x) nil)
			 (destroy x))
		     (callbacks m))
	     (call-next-method)))))

;; It remains to be seen if this works
(defmethod destroy ((m g-objmeta))
  (debugf t "Here we go destroying a g-objmeta~%")
  (let ((standing (destroyers m)))
    (cond ((and standing (destroy-real-object m))
	   (debugf t "It is still standing.~%")
	   (mapcar #'destroy (callbacks m))
	   (debugf t "Callbacks deallocated.~%")
	   (mapcar #'destroy (destroyers m))
	   (debugf t "Destroyers removed.~%")
	   ;; In particular - is this the right function?
	   (gtk-aliens::|g_object_unref| (contents m))
	   (debugf t "Object killed.~%")
	   (call-next-method))
	  (t (mapcar #'(lambda (x)
			 (setf (retire-p x) nil)
			 (destroy x))
		     (callbacks m))
	     (call-next-method)))))

(defmethod destroy ((c gtk-object-cb-meta))
  (let* ((retire-p (retire-p c))
	 (cap (capsule c))
	 (wid (if cap (object cap))))

    (if (and retire-p wid)
	(gtk-aliens::|g_signal_handler_disconnect|
		     (contents wid) (contents c)))
    (call-next-method)))

(defmethod destroy ((c gtk-timeout-callback))
  (if (retire-p c)
      (gtk-aliens::|gtk_timeout_remove| (contents c)))
  (call-next-method))

(defmethod destroy ((c gtk-idle-callback))
  (if (retire-p c)
      (gtk-aliens::|gtk_idle_remove| (contents c)))
  (call-next-method))

(defmacro def-callback-type (name marker decoy)
  `(defresource ,name
     (callback-resource :marker ,marker
			:trampoline (c-fun-ptr ,decoy))))

(defun getanumber ()
  (format t "Enter a number to be returned to the toolkit, e.g 42~%")
  (list (read)))

(defun booltrans (v)
  (typecase v
    (fixnum v)
    (t (if v 1 0))))

(defun straynotify (cookie)
  (debugf t "WARNING: Stray callback trapped! cookie=~a~%"
	  cookie)
  0)

;; Whether we are tunneling an error through the main-loop
(defvar *tunnel* nil)

;; Whether we are in the main loop (not in lisp-land)
(defvar *in-main* nil)

;; Whether the main loop is running (lisp-land or not)
(defvar *main-active* nil)

;; The sigint handler.
(defvar *sigint-handler* nil)

;; When the user "returns to top-level", we stop here, and unwind the
;; callstack for the toolkit "the proper way", that is, by
;; returning. The main loop then throws again the "return to toplevel"
;; tag.
(defmacro tunnel-on-abort (&rest code)
  (let ((dum (gensym)))
    `(let ((,dum (quote ,dum)))
       (catch (quote common-lisp::top-level-catcher)
	 (setf ,dum
	       (progn ,@code)))
       (cond ((eql ,dum (quote ,dum))
	      (setf *tunnel* t)
	      (gtk-aliens::|gtk_main_quit|)
	      0)
	     (t ,dum)))))


;;; Assorted handlers.
(defmacro %defhandler (name vars &rest body)
  (let ((val (gensym)))
  `(defun ,name ,vars
;; doesnt work:
;; (system:with-enabled-interrupts ((unix:SIGINT gtknexus::*sigint-handler*))
     (let ((*in-main* nil))
     (tunnel-on-abort
      (let ((,val 0))
	(restart-case
	 ;; Proper handler code
	 (setf ,val (progn ,@body))

	 (return-zero ()
		      :report "Return 0 to toolkit."
		      (setf ,val 0))

	 (return-val (something)
		     :report "Return something else to toolkit."
		     :interactive getanumber
		     (setf ,val something)))

	(booltrans ,val)))))))

;; Fill in
(%defhandler %standard-handler (rw cookie)
  (debugf t "%standard-handler got cookie ~a.~%" cookie)
  (let* ((cbmeta (gethash cookie (table *gtkcallbacks*)))
	 (cbcap (if cbmeta (capsule cbmeta))))
    (if (not cbcap) (straynotify cookie)
      (funcall (func cbcap)
	       (object cbcap)
	       (data cbcap)))))

(%defhandler %destroy-handler (rw cookie)
  (debugf t "%destroy-handler got cookie ~a.~%" cookie)
  (let* ((cbmeta (gethash cookie (table *gtkcallbacks*)))
	 (cbcap (if cbmeta (capsule cbmeta))))

    (if (not cbcap) (straynotify cookie)
      (let* ((wid (object cbcap))
	     (dest (remove (meta cbcap)
			   (destroyers (meta wid)))))
	(funcall (func cbcap)
		 (object cbcap)
		 (data cbcap))
	(setf (retire-p cbmeta) nil)
	(destroy cbcap)
	(setf (destroyers (meta wid)) dest)

	(unless dest
	  (debugf t "Object's life is over.~%")
	  (destroy wid)))))) ;; destroy internaly.

(%defhandler %event-handler (rw ev cookie)
  (debugf t "%event-handler got cookie ~a.~%" cookie)
  (let* ((cbmeta (gethash cookie (table *gtkcallbacks*)))
	 (cbcap (if cbmeta (capsule cbmeta))))
    (if (not cbcap) (straynotify cookie)
      (funcall (func cbcap)
	       (object cbcap)
	       ev ;; Might get encapsulated in the future.
	       (data cbcap)))))

;; Handling of idle and timeout calls. Slight but significant
;; variations on the above.

(%defhandler %itc-handler (cookie)
  (debugf t "%itc-handler got cookie ~a.~%" cookie)
  (let* ((cbmeta (gethash cookie (table *gtkcallbacks*)))
	 (cbcap (if cbmeta (capsule cbmeta))))
    (if (not cbcap) (straynotify cookie)
      (let ((r (booltrans (funcall (func cbcap)
				   (data cbcap)))))
	(if (= r 0) (destroy (meta cbcap)))   ;; r=0 means don't call
					      ;; me again.

	r))))

;; Trampolines
(def-c-callable gtk-standard-decoy (:void (w (* t)) (cookie :int))
  (%standard-handler w cookie))

(def-c-callable gtk-destroy-decoy (:void (w (* t)) (cookie :int))
  (%destroy-handler w cookie))

(def-c-callable gtk-evhandling-decoy (:int (w (* t)) (ev (* t)) (cookie :int))
  (%event-handler w ev cookie))

(def-c-callable %gtk-itc-handler (:int (id :int))
  (%itc-handler id))
