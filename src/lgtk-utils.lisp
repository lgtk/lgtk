;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) March 2004 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;; In this file belong all the "lispy" extensions to gtk.
(defpackage #:lgtk-utils
  (:export :make-gtk-button)
  (:use :common-lisp :gtk))

(in-package #:lgtk-utils)

;; A function for creating GTK buttons of various variants.
(defun make-gtk-button (&optional (key :none) data)
  (ecase key
    (:none (gtk-button-new))
    (:label (gtk-button-new-with-label data))
    (:from-stock (gtk-button-new-from-stock data))
    (:mnemonic (gtk-button-new-with-mnemonic data))))
