;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;; Widgets and callbacks
(defpackage :widget-nexus
  (:export :metawidget
	   :widcapsule
	   :callbacks
	   :destroyers
	   :sapcapsule
	   :sapmeta
	   :resource
	   :callback-resource
	   :marker
	   :trampoline
	   :defresource
	   :metacallback
	   :callback-capsule
	   :callback-mixin
	   :func :data
	   :idle-callback
	   :timeout-callback
	   :widget-callback
	   :dummy-func
	   :retire-p)
  (:use common-lisp nexus clnexus-port))

(in-package :widget-nexus)

(defclass sapmeta (metacapsule) ())
(defclass sapcapsule (weak-capsule)
    ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'sapmeta))))

(defmethod metacapsule-identify ((m sapmeta))
  (alien-address (contents m)))

(defclass metawidget (sapmeta)
  ((callbacks :accessor callbacks
	      :initform nil)
   (destroyers :accessor destroyers
	       :initform nil)))

(defclass widcapsule (weak-capsule)
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'metawidget))))

(defclass resource ()
  ((marker :reader marker
	   :initarg :marker)))

(defclass callback-resource (resource)
  ((trampoline :reader trampoline
	       :initarg :trampoline)))

(defmacro defresource (name (class &rest args))
  `(defparameter ,name (make-instance (quote ,class) ,@args)))

(defclass metacallback (idmeta)
  ;; Whether to ask the external program to delete the callback. Set
  ;; to nil when for example the associated widget is in the process
  ;; of being destroyed.
  ((retire-p :accessor retire-p
	     :initarg :retire-p
	     :initform t)))

(defclass callback-mixin ()
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'metacallback))
   (func :accessor func
	 :initarg :func
	 :initform nil)
   (data :accessor data
	 :initarg :data
	 :initform nil)))

;; For example
;;(defclass widget-callback (callback-mixin weak-idcapsule)
;;  ((widget :accessor widget
;;	   :initarg :widget
;;	   :initform :nil)))

(defmethod retire ((c callback-mixin))) ;; empty.

(defmethod destroy ((m metacallback))
  (let ((c (capsule m))
	(retire-p (retire-p m)))
    (if (and c retire-p) (retire c))     ;; "remove" the callback, if
					 ;; that is still posible
    (call-next-method)))

;; A usefull filler
(defun dummy-func (&rest args)
  (declare (ignore args))
  nil)
