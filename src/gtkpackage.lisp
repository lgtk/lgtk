;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(defpackage :gtk
  (:export ;; Events and signals
           :gtkdelete-event :gtkdestroy :gtkclicked :gtktoggled
	   :gtkvalue-changed

	   ;; Signal stuff
	   :g-signal-connect :g-signal-connect-swapped

	   ;; constructors
	   :gtk-window-new

	   :gtk-label-new

	   :gtk-hbox-new :gtk-vbox-new

	   :gtk-table-new

	   :gtk-hseparator-new

	   :gtk-image-new
	   :gtk-image-new-from-file

	   :gtk-button-new
	   :gtk-button-new-with-label
	   :gtk-button-new-with-mnemonic
	   :gtk-toggle-button-new
	   :gtk-toggle-button-new-with-label
	   :gtk-toggle-button-new-with-mnemonic
	   :gtk-check-button-new
	   :gtk-check-button-new-with-label
	   :gtk-check-button-new-with-mnemonic

	   ;; The rest
	   :gtk-main :gtk-widget-show :gtk-widget-set-size-request
	   :gtk-container-add :gtk-box-pack-start :gtk-box-pack-end
	   :gtk-container-set-border-width :gtk-window-set-title
	   :gtk-main-quit :gtk-main-quit :gtk-widget-destroy
	   :gtk-misc-set-alignment :shash :gtk-timeout-add :gtk-idle-add
	   :gtk-timeout-remove :gtk-idle-remove :gtk-table-attach
	   :gtk-toggle-button-get-active :gtk-toggle-button-set-active

	   ;; enums. Only export those which can be combined via | in
	   ;; C. See file enums.lisp for details.
	   :gtkattachoptions)
  (:use common-lisp nexus widget-nexus enums defbinding clnexus-port
	dynaslot))
