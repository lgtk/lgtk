;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;; Portablility package.
(defpackage #:clnexus-port
  (:export #:alien-address #:finalize #:make-weak-pointer
	   #:weak-pointer-value #:*weak-pointer-type* #:run-after-gc
	   #:def-alien-routine #:port-alien-type #:def-c-callable #:c-fun-ptr
	   #:swap-unix-sigint-handler #:voidptr #:peek #:poke)
  #+cmu (:use common-lisp ext system alien c-call)
  #+sbcl (:use common-lisp sb-ext sb-sys sb-alien callback)
  (:shadow def-callback def-alien-routine finalize make-weak-pointer
	   weak-pointer-value))

(in-package #:clnexus-port)

(defmacro def-callback (&rest args)
  #+(and cmu #.(cl:if (cl:find-package "CALLBACK") '(and) '(or)))
  `(callback:defcallback ,@args)
  #+(and cmu #.(cl:if (cl:find-package "CALLBACK") '(or) '(and)))
  `(alien:def-callback ,@args)
  #+sbcl `(defcallback ,@args))

'(defmacro def-c-types (&rest stuff)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ,@(mapcar #'(lambda (x)
		   `(setf (get ',(car x) 'alien-type)
			  ',(cadr x)))
	       stuff)))

;;; Alien magic

;; basic C types
(defparameter *c-types*
  '((:char char)
    (:short short)
    (:ushort unsigned-short)
    (:int int)
    (:uint unsigned-int)
    (:long long)
    (:ulong unsigned-long)
    (:double double)
    (:float float)

    (:c-string c-string)

    (:void void)
    (:voidptr (* t))
    (* *)
    (t t))
  "Maps `port' C type specifiers from the :KEYWORDS package to symbols
from the the C-CALL (CMUCL) or SB-ALIEN (SBCL) packages.")

(defun port-alien-type (key)
  (let ((it (cond ((atom key) (cadr (assoc key *c-types*)))
		  (t (mapcar #'port-alien-type key)))))
    (if it it
      (warn "~A not is not an alien type" key))))

;; Get the actual pointer number
(defun alien-address (it)
  (sap-int (alien-sap it)))

(defmacro def-alien-routine (&rest stuff)
  #+cmu `(alien:def-alien-routine ,@stuff)
  #+sbcl `(define-alien-routine ,@stuff))

;;; GC magic

(defvar *weak-pointer-type* 'weak-pointer)

(defun finalize (fun obj)
  #+cmu (ext:finalize fun obj)
  #+sbcl (sb-ext:finalize fun obj))

(defun make-weak-pointer (obj)
  #+cmu (ext:make-weak-pointer obj)
  #+sbcl (sb-ext:make-weak-pointer obj))

(defun weak-pointer-value (obj)
  #+cmu (ext:weak-pointer-value obj)
  #+sbcl (sb-ext:weak-pointer-value obj))

(defun run-after-gc (fun)
  (pushnew fun *after-gc-hooks*))

(defun voidptr (int)
  (int-sap int))

(defun swap-unix-sigint-handler (new-one)
  #+cmu (system:enable-interrupt unix:SIGINT new-one)
  #+sbcl (sb-sys:enable-interrupt sb-unix:SIGINT new-one))

;;; Aaaah BASIC! Those where the days.....
#+cmu
(defmacro peek (base off type)
  `(alien:deref
     (alien:sap-alien
      (system:int-sap
       (+ ,off
	  (system:sap-int (alien:alien-sap ,base))))
      (* ,type))))

#+sbcl
(defmacro peek (base off type)
  `(sb-alien:deref
     (sb-alien:sap-alien
      (sb-sys:int-sap
       (+ ,off
	  (sb-sys:sap-int (sb-alien:alien-sap ,base))))
      (* ,type))))

#+cmu
(defmacro poke (base off type value)
  `(setf (alien:deref
	  (alien:sap-alien
	   (system:int-sap
	    (+ ,off
	       (system:sap-int (alien:alien-sap ,base))))
	   (* ,type))) ,value))

#+sbcl
(defmacro poke (base off type value)
  `(setf (sb-alien:deref
	  (sb-alien:sap-alien
	   (sb-sys:int-sap
	    (+ ,off
	       (sb-sys:sap-int (sb-alien:alien-sap ,base))))
	   (* ,type))) ,value))

;; The callback interface will get non-portable among sbcl and cmucl.
(defmacro def-c-callable (name (return-type &rest arg-specs) &rest body)
  "Defines a callback using the `port' C type specifiers. Uses
PORT-ALIEN-TYPE to convert the syntax to the implementation-dependant
alien type specifiers."
  `(def-callback ,name 
    ,(list* (port-alien-type return-type)
	    (mapcar (lambda (arg-spec)
		      (destructuring-bind (name type) arg-spec
			(list name (port-alien-type type))))
		    arg-specs))
    ,@body))

(defmacro c-fun-ptr (it)
  `(callback ,it))
