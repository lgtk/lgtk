;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(in-package #:gtk)

(defclass gtk-idle-cb-meta (metacallback) ())
(defclass gtk-idle-callback (callback-mixin idcapsule)
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'gtk-idle-cb-meta))))

(defclass gtk-timeout-cb-meta (metacallback) ())
(defclass gtk-timeout-callback (callback-mixin idcapsule)
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'gtk-timeout-cb-meta))))

(defclass gtk-object-cb-meta (metacallback) ())
(defclass gtk-object-callback (callback-mixin weak-idcapsule)
  ((object :accessor object
	   :initarg :object
	   :initform nil)
   (meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'gtk-object-cb-meta))))
;  (:default-initargs (:meta (find-class 'gtk-object-cb-meta))))
;   (meta :accessor meta
;	 :initarg :meta
;	 :initform (find-class 'gtk-object-cb-meta))))

(defclass gtk-objmeta (metawidget) ())
(defclass gtk-objcapsule (widcapsule)
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'gtk-objmeta))))

(defclass g-objmeta (metawidget) ())
(defclass g-objcapsule (widcapsule)
  ((meta :accessor meta
	 :initarg :meta
	 :initform (find-class 'g-objmeta))))

;;;
;;; Here we import the complete gtk class hierarchy.
;;;
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro def-capsule-filter (type)
    `(defun ,type (x)
       (cond ((not x) nil)
	     (t (assert (typep x ',type))
		(contents x)))))

  (defmacro def-encapsulator (name type)
    `(defun ,name (x &key (destroy t))
       (alien-encapsulate x ',type :destroy destroy)))

  (defun gencap (symb)
    (intern (format nil "~A-ENCAP" symb)))

  (defun generate-class-hierarchy (base h)
    (let ((buf nil))
      (labels ((aux (h base)
		    (let ((name-symbol (read-from-string (car h))))
		      (push `(defclass ,name-symbol (,base) ()) buf)
		      (push `(def-capsule-filter ,name-symbol) buf)
		      (push `(export ',name-symbol) buf)
		      (push `(def-encapsulator
			       ,(gencap name-symbol)
			       ,name-symbol) buf)
		      (push `(def-binding-type ,name-symbol
			       :in ',name-symbol
			       :out ',(gencap name-symbol)
			       :alien '(* t)) buf)
		      (mapcar #'(lambda (x) (aux x name-symbol)) (cdr h)))))
	(aux h base)
	(let ((xxx (reverse buf)))
	  xxx))))


  ;; We got this information as it is from the tutorial.
  (defmacro make-gtk-object-hierarchy ()
    `(progn    ;(eval-when (:compile-toplevel :load-toplevel :execute)
       ,@(generate-class-hierarchy
	  'gtk-objcapsule
	  '("GtkObject"
	    ("GtkWidget"
	     ("GtkMisc"
	      ("GtkLabel"
	       ("GtkAccelLabel"))
	      ("GtkArrow")
	      ("GtkImage"))
	     ("GtkContainer"
	      ("GtkBin"
	       ("GtkAlignment")
	       ("GtkFrame"
		("GtkAspectFrame"))
	       ("GtkButton"
		("GtkToggleButton"
		 ("GtkCheckButton"
		  ("GtkRadioButton")))
		("GtkOptionMenu"))
	       ("GtkItem"
		("GtkMenuItem"
		 ("GtkCheckMenuItem"
		  ("GtkRadioMenuItem"))
		 ("GtkImageMenuItem")
		 ("GtkSeparatorMenuItem")
		 ("GtkTearoffMenuItem")))
	       ("GtkWindow"
		("GtkDialog"
		 ("GtkColorSelectionDialog")
		 ("GtkFileSelection")
		 ("GtkFontSelectionDialog")
		 ("GtkInputDialog")
		 ("GtkMessageDialog"))
		("GtkPlug"))
	       ("GtkEventBox")
	       ("GtkHandleBox")
	       ("GtkScrolledWindow")
	       ("GtkViewport"))
	      ("GtkBox"
	       ("GtkButtonBox"
		("GtkHButtonBox")
		("GtkVButtonBox"))
	       ("GtkVBox"
		("GtkColorSelection")
		("GtkFontSelection")
		("GtkGammaCurve"))
	       ("GtkHBox"
		("GtkCombo")
		("GtkStatusbar")))
	      ("GtkFixed")
	      ("GtkPaned"
	       ("GtkHPaned")
	       ("GtkVPaned"))
	      ("GtkLayout")
	      ("GtkMenuShell"
	       ("GtkMenuBar")
	       ("GtkMenu"))
	      ("GtkNotebook")
	      ("GtkSocket")
	      ("GtkTable")
	      ("GtkTextView")
	      ("GtkToolbar")
	      ("GtkTreeView"))
	     ("GtkCalendar")
	     ("GtkDrawingArea"
	      ("GtkCurve"))
	     ("GtkEditable"
	      ("GtkEntry"
	       ("GtkSpinButton")))
	     ("GtkRuler"
	      ("GtkHRuler")
	      ("GtkVRuler"))
	     ("GtkRange"
	      ("GtkScale"
	       ("GtkHScale")
	       ("GtkVScale"))
	      ("GtkScrollbar"
	       ("GtkHScrollbar")
	       ("GtkVScrollbar")))
	     ("GtkSeparator"
	      ("GtkHSeparator")
	      ("GtkVSeparator"))
	     ("GtkInvisible")
	     ("GtkPreview")
	     ("GtkProgressBar"))
	    ("GtkAdjustment")
	    ("GtkCellRenderer"
	     ("GtkCellRendererPixbuf")
	     ("GtkCellRendererText")
	     ("GtkCellRendererToggle"))
	    ("GtkItemFactory")
	    ("GtkTooltips")
	    ("GtkTreeViewColumn")))))

  ;; engage.
  (make-gtk-object-hierarchy))
