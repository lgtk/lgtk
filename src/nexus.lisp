;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

;; A framework for keeping keeping foreign objects in a lispy way

(defpackage :nexus
  (:export :debugf
	   :simple-capsule
	   :capsule
	   :contents
	   :metacapsule
	   :metacapsule-identify
	   :destroy-real-object
	   :bag
	   :meta
	   :nexus
	   :gcconnect
	   :destroy
	   :get-id
	   :forget-id
	   :weak-capsule
	   :table
	   :attach
	   :id
	   :idmeta
	   :weak-idcapsule
	   :idnexus
	   :idcapsule)

  (:use common-lisp clnexus-port))

(in-package :nexus)

;; Facility for tracing the gc stuff
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *add-debug-code* t)

  (defvar *debug* nil)

  (defvar *efence* t)) ;; Expensive safety code that helps track
		       ;; subtle errors

(defmacro debugf (&rest stuff)
  (if *add-debug-code*
      `(if *debug*
	   (format ,@stuff))))

;; A simple capsule. To keep foreign data safe.
(defclass simple-capsule ()
  (;; The real, live, dangerous object
   (real-object :accessor contents
		:initarg :contents)
   ;; Things needed by the object to work should not be inadvertently
   ;; GCed while there are references to the capsule. Thus they are
   ;; stuffed into the bag.
   (bag :accessor bag
	:initform nil)))

;; Some real objects live a dual reality. Once in the hand of users...
(defclass capsule (simple-capsule)
   ;; Data related to the behind-the-scenes side of the story
  ((meta :accessor meta
	 :initarg meta
	 :initform (find-class 'metacapsule))))

;; ...and once in the hand of the framework.
(defclass metacapsule ()
  (;; Again the real object
   (real-object :accessor contents
		:initarg :contents)
   ;; The user capsule. We may manipulate it in case the real object
   ;; gets "destroyed". We will define the accesor in a second
   (capsule :initarg :capsule
	    :initform nil)

   ;; Do we destroy this on GC? Good question. On by default.
   (destroy-real-object :accessor destroy-real-object
			:initarg :destroy-real-object
			:initform T)

   ;; The nexus keeps a reference to it. Needed for bookkeeping.
   (nexus :accessor nexus
	  :initarg :nexus
	  :initform nil)))

;; capsule accessor. Makes the weak-pointer hairy thing transparent
(defmethod capsule ((m metacapsule))
  (debugf t "see my capsule.~%")
  (let ((pc (slot-value m 'capsule)))
    (if (typep pc *weak-pointer-type*)
	(weak-pointer-value pc)
      (values pc t))))

;; Who are you?
(defmethod metacapsule-identify ((m metacapsule))
  m)

(defmethod (setf capsule) ((c capsule) (m metacapsule))
  (setf (slot-value m 'capsule) c))

(defun (setf foo) (a b)
  (format t "~S ~S~%" a b))

(defmethod initialize-instance :after ((c capsule)
				       &key contents nexus destroy-real-object)
  (setf (meta c)
	(make-instance (meta c)
		       :contents contents
		       :capsule c
		       :nexus nexus
		       :destroy-real-object destroy-real-object)))

;; Only defined on metas, but the user is king.
(defmethod destroy ((c capsule))
  (destroy (meta c)))

;; connect things so that they are not gced inadvertently.
(defmethod gcconnect ((c1 capsule) (c2 capsule))
  (pushnew c1 (bag c2))
  (pushnew c2 (bag c1))
  t)

(defmethod gcconnect ((c capsule) (o t))
  (pushnew o (bag c)))

(defmethod gcconnect ((o t) (c capsule))
  (pushnew o (bag c)))


;; A weak capsule is one that gets properly GCed when "forgotten" by the user.
(defclass weak-capsule (capsule) ())

(defmethod (setf capsule) ((c weak-capsule) (m metacapsule))
  (debugf t "Weak!~%")
  (setf (slot-value m 'capsule) (make-weak-pointer c)))

(defvar *nexus-gc-hook* #'(lambda ())) ;; The nop.

(defmethod initialize-instance :after ((c weak-capsule)
				       &key)

  (let ((meta (meta c)))

    (setf (capsule meta) c)

    (debugf t "Scheduling ~a for gc.~%" c)
    (finalize c
	      #'(lambda ()
		  (debugf t "Triggering destroy on ~A.~%" meta)
		  (destroy meta)
		  (debugf t "Destroy finished. Calling hook.~%")
		  (funcall *nexus-gc-hook*)
		  (debugf t "Done.~%")))))

;; All this only makes sense if the meta-capsules are kept in some
;; global structure with a firm attachment.
(defclass nexus ()
  ((table :accessor table
	  :initform (make-hash-table))))

;; Attach works only on metacapsules, but the user is king.
(defmethod attach ((c capsule) (n nexus))
  (attach (meta c) n))

(defmethod attach ((meta metacapsule) (n nexus))
  (debugf t "Attach called with nexus ~a.~%" n)
  (when n  ;; This is optional
    (setf (nexus meta) n) ;; where am I?
    (setf (gethash (metacapsule-identify meta)
		   (table n))
	  meta)))

(defmethod initialize-instance :after ((m metacapsule) &key nexus)
  (attach m nexus))

;; Sometimes we need ids for objects so that we can pass around
;; references to simple-minded C-creatures.
(defvar *id-jar* nil)
(defvar *id-top* 0)

(defun get-id ()
  (if *id-jar*
      (pop *id-jar*)
    (incf *id-top*)))

(defun forget-id (id)
  (if (and *efence* (find id *id-jar*))
      (error "Two identical IDs have been disposed!")
    (push id *id-jar*)))

(defclass meta-id-mixin ()
  ((id :accessor id
       :initarg :id
       :initform nil)))

(defclass idmeta (meta-id-mixin metacapsule) ())

(defclass capsule-id-mixin ()
  ((meta :accessor meta
	 :initarg meta
	 :initform (find-class 'idmeta))))

(defclass weak-idcapsule (capsule-id-mixin weak-capsule) ())

(defclass idcapsule (capsule-id-mixin capsule) ())

(defmethod id ((c capsule-id-mixin))
  (id (meta c)))

;; A nexus in which objects are kept by id number.
(defclass idnexus (nexus) ())

(defmethod metacapsule-identify ((meta idmeta))
  (let ((id (id meta)))
    (if id id
      (let ((id (get-id)))
	(setf (slot-value meta 'id) id)
	id))))

;; Standard destroy methods. Like this they would not make much sense.
(defmethod destroy ((meta metacapsule))
  (let ((n (nexus meta)))
    (if n (remhash (metacapsule-identify meta)
		   (table n)))

    (debugf t "Removed ~a from nexus ~a.~%" meta n))
  (setf (destroy-real-object meta) nil))

(defmethod destroy ((meta idmeta))
  (let ((id (id meta)))
    (cond (id (debugf t "Removing id ~a from nexus.~%" (id meta))
	      (remhash (id meta) (table (nexus meta)))
	      (forget-id (id meta))
	      (setf (id meta) nil))
	  (t (debugf t "idmeta redestroyed.~%"))))
  (setf (destroy-real-object meta) nil))
