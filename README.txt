
		  lgtk, GTK bindings for Common lisp
		======================================

 Contents:
	1. Introduction
	2. Usage
	3. What to do when things go wrong

1. Introduction

lgtk is a library for writing graphical user interfaces with the GTK+
2.0, 2.2 etc. toolkit. The goals of the design were

    * Easy installation and usage - full support from the ASDF system
      definition facility.

    * Provide an interface which resembles as closely as possible the
      original GTK interface. Up to some obvious and regular changes,
      you can use standard GTK documentation.

    * GC support. Stray widgets and closures get properly
      garbage-collected, and if applicable, unmapped. Obviously this
      does not mean that you do not add proper destroy mechanisms to
      your apps.

    * Full error recovery. Lisp programmers expect their lisp image to
      stay stable even after many errors, breaks, restarts,
      etc. lgtk tries to accommodate this to the fullest extent
      possible.

Future goals:

    * Multithreading support.

    * Portablilty. For now it only runs on CMUCL and SBCL. It should
      not be too difficult to port to other implementations, though.

    * Add missing functionality. That is quite a bit at the moment.

The code comes under the terms of the modified BSD license ("sans
advertising clause") and without any warranty. See the file COPYING
for details.

1. Usage

You should be able to use gtk almost as in C. You can create widgets,
add and remove callbacks, timeouts, and idle calls, etc.

The difference is that you do it in Lisp. :-)

[Note: at the moment the functionality offered by lgtk is only a small
subset of gtk]

Functions have the same names as in C, except that underscores have
been replaced by hyphens. To get a feeling for how lgtk is used, you
should look at the GTK tutorial, and contrast the examples there with
the corresponding counterpart in the examples subdirectory of the lgtk
tarball.

The main differences are:

    * Arguments which specify options, and which can be set to
      reasonable defaults, have been substituted by keyword args. The
      same applies to the `data' arguments of callback scheduling
      functions.

    * There are no bindings to NIH features. Because of the previous
      difference, functions ending with _defaults are unnecessary.

    * Callbacks and signals cannot be specified by string name. A
      constant is used for this, which has the same name but is
      prefixed by gtk. To specify signal "clicked" you pass the
      constant named `gtkclicked'.

Again, see the examples to get a feeling for all this.

2. What to do when things go wrong.

If a callback lands you in the lisp debugger, you have the option of
returning a value to the toolkit and continue operation normally. See
the debugging options that appear when this happens (you can of course
follow any of the other options)

If your program hangs, or if you just feel like it, you can interrupt
it with Control-C, and return to the lisp top level.

The next potential problem becomes apparent. We are back at the
prompt, but the window is still open, and does not respond. You have
three options:

      a. Move on. Eventually the widgets get GCed, and the window
	 disappears silently.

      b. Trigger gc. Type

	   (ext:gc :full t)

	 This doesn't always work, though, because the :full isn't all
	 that :full.

      c. [not yet implemented] Nuke the toolkit. Type
	 
	 (reset-gtk-toolkit)

      d. Enter the main loop again.

         (gtk-main)

	 and close the app (if that works).

----
(c) 2003 Mario S. Mommer <mommer@well-of-wyrd.net>
