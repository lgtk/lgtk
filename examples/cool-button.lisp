;; A button with a pixmap.
(use-package :gtk)

(defun cool-button ()
  (labels ((xpm-label-box (fname text)
	      (let ((box (gtk-hbox-new))
		    (image (gtk-image-new-from-file fname))
		    (label (gtk-label-new text)))
		(gtk-container-set-border-width box 2)
		(gtk-box-pack-start box image)
		(gtk-box-pack-start box label)

		(gtk-widget-show image)
		(gtk-widget-show label)
		box))

	   (hello (wid data)
		  (format t "Hi, ~A was pressed.~%" data))

	   (the-end (&rest args) (declare (ignore args))
		    (gtk-main-quit)))

    (let ((window (gtk-window-new :gtk-window-toplevel))
	  (button (gtk-button-new))
	  (box (xpm-label-box "info.xpm" "cool button")))
    
      (gtk-window-set-title window "Pixmap'd Buttons!")
      (gtk-container-set-border-width window 10)
      
      (gtk-container-add window button)
      (gtk-container-add button box)

      (g-signal-connect button gtkclicked #'hello :data "cool button")
      (g-signal-connect window gtkdestroy #'the-end)
      (g-signal-connect window gtkdelete-event #'the-end)

      (gtk-widget-show box)
      (gtk-widget-show button)
      (gtk-widget-show window)

      (gtk-main))))