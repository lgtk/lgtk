(use-package :gtk)

(defun splash-msg (msg msec)
"Displays the string msg in a single window, for msec milliseconds."
  (let ((window (gtk-window-new :gtk-window-toplevel))
	(label (gtk-label-new msg)))

    (labels ((out (&rest args)
		  (declare (ignore args))
		  (gtk-main-quit)
		  0)
	     (over (data)
		   (declare (ignore data))
		   (gtk-widget-destroy window)
		   0))

    (gtk-container-add window label)

    (g-signal-connect window gtkdestroy #'out)

    (gtk-widget-show label)
    (gtk-widget-show window)

    (gtk-timeout-add msec #'over)

    (gtk-main))))