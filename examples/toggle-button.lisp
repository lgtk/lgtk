(use-package :gtk)

(defun toggle-button ()
  (labels ((hw (widget data)
	       (if (gtk-toggle-button-get-active widget)
		   (format t "~A is active.~%" data)
		 (format t "~A is not  active.~%" data)))

	   (the-end (&rest args)
		    (declare (ignore args))
		    (gtk-main-quit)))
    (let ((window (gtk-window-new :gtk-window-toplevel))
	  (button (gtk-toggle-button-new-with-mnemonic
		   "m_nemonic toggle button")))

      (gtk-container-add window button)
      (gtk-container-set-border-width window 20)

      (gtk-toggle-button-set-active button t)

      (g-signal-connect button gtkclicked #'hw
			:data "toggle-button")

      (g-signal-connect window gtkdestroy #'the-end)

      (gtk-widget-show button)
      (gtk-widget-show window)

      (gtk-main))))