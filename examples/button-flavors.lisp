
;; All kinds of plain buttons
(use-package :gtk)
(use-package :lgtk-utils)

;; Getters for button stuff...
(defun gtk-button-relief (b) (gtk-button-get-relief b))
(defun gtk-button-label (b) (gtk-button-get-label b))

;; ...and setters
(defsetf gtk-button-relief (button) (relief)
  `(progn (gtk-button-set-relief ,button ,relief)
	  ,relief))


(defun button-flavours ()
  (labels
      ((done (&rest args)
	     (declare (ignore args))
	     (gtk-main-quit)
	     0)

       (clicked (wid arg)
                (declare (ignore arg))
		(format t "(~s) '~a'~%"
			(gtk-button-relief wid)
			(gtk-button-label wid)))
		
       (add-button (button box flavour)
		   (g-signal-connect button gtkclicked #'clicked)
		   (gtk-box-pack-start box button
				       :expand t :fill t :padding 10)
		   (setf (gtk-button-relief button) flavour)
		   (gtk-widget-show button))
		   
       (mk-hbox (flavour)
		(let ((hbox (gtk-hbox-new :homogeneous t :spacing 4)))
		  (add-button (make-gtk-button)
			      hbox flavour)
		  (add-button (make-gtk-button :label "Label")
			      hbox flavour)
		  (add-button (make-gtk-button :mnemonic "M_nemonic")
			      hbox flavour)
		  (add-button (make-gtk-button :from-stock "gtk-ok")
			      hbox flavour)
		  (gtk-widget-show hbox)
		  hbox)))
  
  (let ((window (gtk-window-new :gtk-window-toplevel))
	(vbox (gtk-vbox-new :homogeneous t :spacing 4)))

    (dolist (flavour '(:gtk-relief-normal :gtk-relief-half :gtk-relief-none))
      (gtk-box-pack-start vbox (mk-hbox flavour)
			  :expand t :fill t :padding 10))
    (gtk-widget-show vbox)
    
    (gtk-container-add window vbox)

    (g-signal-connect window gtkdelete-event #'done)
    (gtk-window-set-title window "Button Flavours!")
    (gtk-container-set-border-width window 10)
    (gtk-widget-show window)
    
    (gtk-main))))