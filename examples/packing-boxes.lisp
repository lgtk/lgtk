;; The example on packing boxes from the gtk tutorial.  Almost. The
;; function takes the spacing as an argument (keep it small, this can
;; crash gtk). I did not bother with case 3, since it does not test
;; anything new.

(use-package :gtk)

(defun packing-boxes (spc)
  (declare (optimize (debug 3)))
  (let (show-list)
    (labels ((show-later (x) (push x show-list) x)

	     (delete-handler (&rest stuff)
			     (declare (ignore stuff))
			     (gtk-main-quit)
			     0)

	     (make-box (homogeneous
			spacing
			expand
			fill
			padding)

		       (let ((box (gtk-hbox-new :homogeneous homogeneous
						:spacing spacing)))
			 ;; Pull off the lispy thang.

			 (mapcar
			  #'(lambda (x)
			      (let ((button (gtk-button-new-with-label x)))
				(gtk-box-pack-start box button
						    :expand expand
						    :fill fill
						    :padding padding)
				(gtk-widget-show button)))
			  `("(gtk-box-pack" "box" "button"
			    ,(format nil ":expand ~S" expand)
			    ,(format nil ":fill ~S" fill)
			    ,(format nil ":padding ~S)" padding)))

			 box)))

      ;; "Main"
      (let ((window (show-later (gtk-window-new :gtk-window-toplevel)))
	    (vbox (show-later (gtk-vbox-new))))

	(g-signal-connect window gtkdelete-event #'delete-handler)
	(gtk-container-set-border-width window 10)
	(gtk-container-add window vbox)
	(gtk-window-set-title window "packing boxes")

	(dolist (homogeneous '(nil t))

	  (let ((label (show-later
			(gtk-label-new
			 (format nil
				 "(gtk-hbox-new :homogeneous ~a :spacing ~a)"
				 homogeneous spc)))))

	    (gtk-misc-set-alignment label 0 0)
	    (gtk-box-pack-start vbox label)
	    (gtk-widget-show label))

	  ;; Get lispy!
	  (mapcar
	   #'(lambda (box)
	       (gtk-box-pack-start vbox box)
	       (gtk-widget-show box))
	   (mapcar
	    #'make-box
	    ;; homogeneous
	    (list homogeneous homogeneous homogeneous)

	    ;; spacing
	    (list spc spc spc)

	    ;; expand
	    '(nil t t)

	    ;; fill
	    '(nil nil t)

	    ;; padding
	    '(0 0 0)))

	  (gtk-box-pack-start vbox
			      (show-later (gtk-hseparator-new))
			      :fill t :padding 5))

	(gtk-box-pack-start vbox
			    (show-later (gtk-hseparator-new))
			    :fill t :padding 5)
	  
	(let ((box (show-later (gtk-hbox-new)))
	      (button (show-later (gtk-button-new-with-label "(quit)"))))

	  (gtk-box-pack-start vbox box)
	  (gtk-box-pack-start box button :expand t)
	  (g-signal-connect-swapped button gtkclicked
				    #'(lambda (x)
					(gtk-widget-destroy window)
					(gtk-main-quit))))

	(mapcar #'gtk-widget-show show-list)

	(gtk-main)))))