;; A hello world using tables
(use-package :gtk)

(defun tables-hw ()
  (labels ((hw (wid data)
	       (format t "Hi. ~A was pressed.~%" data))

	   (destroy (&rest args)
		    (format t "self-destruct.~%")
		    (gtk-main-quit)))

    (let ((window (gtk-window-new :gtk-window-toplevel))
	  (button1 (gtk-button-new-with-label "button 1"))
	  (button2 (gtk-button-new-with-label "button 2"))
	  (quitbut (gtk-button-new-with-label "Quit"))
	  (table   (gtk-table-new 2 2 :homogeneous t)))

      (gtk-window-set-title window "Table")
      (gtk-container-set-border-width window 10)
      (gtk-container-add window table)

      (gtk-table-attach table button1 0 1 0 1)
      (gtk-table-attach table button2 1 2 0 1)
      (gtk-table-attach table quitbut 0 2 1 2)

      (g-signal-connect button1 gtkclicked #'hw :data 1)
      (g-signal-connect button2 gtkclicked #'hw :data 2)
      (g-signal-connect window gtkdestroy #'destroy)
      (g-signal-connect-swapped quitbut gtkclicked
				#'gtk-widget-destroy
				:data window)

      (gtk-widget-show button1)
      (gtk-widget-show button2)
      (gtk-widget-show quitbut)
      (gtk-widget-show table)
      (gtk-widget-show window)

      (gtk-main))))
