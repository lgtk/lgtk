;; Hello World #1 from the GTK+ tutorial. This one runs in the
;; background. My advice: don't do it.

(use-package :gtk)

(defun mp-hello-world ()
  (let (idle-tag)
    (labels ((hw (&rest args)
		 (format t "Hello world!~%"))

	     (delete-event (&rest args)
			   (format t "delete-event ocurred~%")
			   0)

	     (idle (data) (declare (ignore data))
		   (mp:process-yield)
		   t)

	     (destroy (&rest args)
		      (gtk-idle-remove idle-tag)
		      (format t "self-destruct.~%")
		      (gtk-main-quit)))

      (let ((window (gtk-window-new :gtk-window-toplevel))
	    (button (gtk-button-new-with-label "Hello World!")))

	(gtk-container-add window button)
	(gtk-container-set-border-width window 10)

	(gtk-widget-show button)
	(gtk-widget-show window)

	(g-signal-connect button gtkclicked #'hw)
	(g-signal-connect window gtkdelete-event #'delete-event)
	(g-signal-connect window gtkdestroy #'destroy)

	(setf idle-tag (gtk-idle-add #'idle))

	(mp:make-process #'gtk-main)))))
