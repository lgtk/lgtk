;; Radio button example
(use-package :gtk)

(defun radio-buttons ()
  (labels ((toggled (wid data)
		    (if (gtk-toggle-button-get-active wid)
			(format t "~A became active.~%" data)))

	   (its-over (&rest a)
		     (declare (ignore a))
		     (gtk-main-quit))

	   (quitcb (mw)
		   (gtk-widget-destroy mw)))

  (let* ((win (gtk-window-new :gtk-window-toplevel))
	 (box (gtk-vbox-new :spacing 10))
	 (b1 (gtk-radio-button-new-with-label nil "Check 1"))
	 (b2 (gtk-radio-button-new-with-label-from-widget b1
							  "Check 2"))
	 (b3 (gtk-radio-button-new-with-label-from-widget b1
							  "Check 3"))
	 (hs (gtk-hseparator-new))

	 (q (gtk-button-new-with-label "Quit")))

    (gtk-container-add win box)

    (gtk-box-pack-start box b1)
    (gtk-box-pack-start box b2)
    (gtk-box-pack-start box b3)
    (gtk-box-pack-start box hs)
    (gtk-box-pack-start box q)

    (g-signal-connect b1 gtktoggled #'toggled :data "#1")
    (g-signal-connect b2 gtktoggled #'toggled :data "#2")
    (g-signal-connect b3 gtktoggled #'toggled :data "#3")

    (g-signal-connect-swapped q gtkclicked #'quitcb :data win)
    (g-signal-connect win gtkdestroy #'its-over)

    (gtk-toggle-button-set-active b1 t)

    (gtk-widget-show q)
    (gtk-widget-show hs)
    (gtk-widget-show b1)
    (gtk-widget-show b2)
    (gtk-widget-show b3)
    (gtk-widget-show box)
    (gtk-widget-show win)

    (gtk-main))))
