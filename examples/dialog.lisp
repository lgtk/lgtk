;; A dialog!

(use-package :gtk)

(defun dialog ()
  (labels ((its-over (&rest args) (declare (ignore args))
		     (gtk-main-quit)))
    (let ((dialog (gtk-dialog-new))
	  (frame (gtk-frame-new "I want your opinion:"))
	  (label (gtk-label-new "Aren't dialogs cool?"))
	  (button1 (gtk-button-new-with-label "Ok..."))
	  (button2 (gtk-button-new-with-label "Well..."))
	  (button3 (gtk-button-new-with-label "Ehm...")))

      (gtk-box-pack-start
       (gtkdialog-action-area dialog)
       button1)

      (gtk-box-pack-start
       (gtkdialog-action-area dialog)
       button2)

      (gtk-box-pack-start
       (gtkdialog-action-area dialog)
       button3)

      (gtk-box-pack-start
       (gtkdialog-vbox dialog)
       frame)
    
      (gtk-container-add
       frame label)

      (gtk-container-set-border-width frame 10)
      (gtk-widget-set-size-request label 30 30)

      (g-signal-connect dialog gtkdestroy #'its-over)

      (gtk-widget-show-all dialog)

      (gtk-main))))