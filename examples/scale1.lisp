(use-package :gtk)

(defun scale1 ()
  (labels ((its-over (&rest a)
		     (declare (ignore a))
		     (gtk-main-quit))

	   (vchange (wid data)
		    (let ((digits (gtk-adjustment-get-value
				   (gtk-range-get-adjustment wid))))

		      (gtk-scale-set-digits data
					    (truncate digits)))))

    (let ((win (gtk-window-new :gtk-window-toplevel))
	  (box (gtk-vbox-new))
	  (scl1 (gtk-hscale-new-with-range 0.0d0 10.0 0.1d0))
	  (scl2 (gtk-hscale-new-with-range 0.0d0 13.0d0 1.0d0)))

      (gtk-container-add win box)
      (gtk-box-pack-start box scl1)
      (gtk-box-pack-start box scl2)

      (gtk-window-set-title win "Hey, this is fun!")
      (gtk-widget-set-size-request scl1 300 50)

      (gtk-scale-set-value-pos scl1 :gtk-pos-bottom)
      (gtk-adjustment-set-value
       (gtk-range-get-adjustment scl2) 0)
      (gtk-scale-set-digits scl1 0)

      (gtk-widget-show scl1)
      (gtk-widget-show scl2)
      (gtk-widget-show box)
      (gtk-widget-show win)

      (g-signal-connect scl2 gtkvalue-changed #'vchange
			:data scl1)

      (g-signal-connect win gtkdestroy #'its-over)

      (gtk-main))))
    