;; Second example from the GTK tutorial.

(use-package :gtk)

(defun hello-world2 ()

  (labels ((callback (wid num)
		     (declare (ignore wid))
		     (format t "Hello again - ~s was pressed.~%" num))

	   (delete-handler (&rest stuff)
			   (declare (ignore stuff))
			   (gtk-main-quit)
			   0)) ;; aka false

    (let ((window (gtk-window-new :gtk-window-toplevel))
	  (button1 (gtk-button-new-with-label "Button 1"))
	  (button2 (gtk-button-new-with-label "Button 2"))

	  ;; This is a bit different. Arguments with a reasonable
	  ;; default where transformed into keyword args.
	  (box (gtk-hbox-new :homogeneous t :spacing 4)))

      ;; Title
      (gtk-window-set-title window "Hello Buttons!")

      ;; Border width
      (gtk-container-set-border-width window 10)

      ;; Put things together
      (gtk-container-add window box)
      (gtk-box-pack-start box button1 :expand t :fill t :padding 10)
      (gtk-box-pack-start box button2)

      ;; Handlers and callbacks
      (g-signal-connect window gtkdelete-event #'delete-handler)
      (g-signal-connect button1 gtkclicked #'callback :data 1)
      (g-signal-connect button2 gtkclicked #'callback :data 2)

      ;; display
      (gtk-widget-show button1)
      (gtk-widget-show button2)
      (gtk-widget-show box)
      (gtk-widget-show window)

      ;; get fancy
      (gtk-main))))