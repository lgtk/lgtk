;; Hello World #1 from the GTK+ tutorial.

(use-package :gtk)

(defun hello-world ()
  (labels ((hw (&rest args)
	       (format t "Hello world!~%"))

	   (delete-event (&rest args)
	       (format t "delete-event ocurred~%")
	       0)

	   (destroy (&rest args)
	       (format t "self-destruct.~%")
	       (gtk-main-quit)))

	  (let ((window (gtk-window-new :gtk-window-toplevel))
		(button (gtk-button-new-with-label "Hello World!")))

	    (gtk-container-add window button)
	    (gtk-container-set-border-width window 10)

	    (gtk-widget-show button)
	    (gtk-widget-show window)

	    (g-signal-connect button gtkclicked #'hw)
	    (g-signal-connect window gtkdelete-event #'delete-event)
	    (g-signal-connect window gtkdestroy #'destroy)

	    (gtk-main))))
