(use-package :gtk)

(defun entry ()

  (labels ((end (&rest a) (declare (ignore a))
		(gtk-main-quit))

	   (ed-toggle (wid data)
		      (gtk-editable-set-editable data
		       (gtk-toggle-button-get-active wid)))

	   (vi-toggle (wid data)
		      (gtk-entry-set-visibility data
		       (gtk-toggle-button-get-active wid)))

	   (get (wid data)
		(format t "Got : ~S~%" 
			(gtk-entry-get-text data))))

    (let ((win (gtk-window-new :gtk-window-toplevel))
	  (vbox (gtk-vbox-new :homogeneous t))
	  (hbox (gtk-hbox-new :homogeneous t))
	  (txt (gtk-entry-new))
	  (ed (gtk-check-button-new-with-label
	       "Editable"))
	  (vi (gtk-check-button-new-with-label
	       "Visible"))
	  (get (gtk-button-new-with-label "Get!")))

      (gtk-container-set-border-width win 10)

      (gtk-container-add win vbox)

      (gtk-box-pack-start vbox txt)
      (gtk-box-pack-start vbox hbox)
      (gtk-box-pack-start vbox get)

      (gtk-box-pack-start hbox ed)
      (gtk-box-pack-start hbox vi)

      (gtk-widget-show-all win)
      
      (g-signal-connect win gtkdestroy #'end)
      (g-signal-connect ed gtkclicked #'ed-toggle :data txt)
      (g-signal-connect vi gtkclicked #'vi-toggle :data txt)
      (g-signal-connect get gtkclicked #'get :data txt)

      (gtk-toggle-button-set-active ed T)
      (gtk-toggle-button-set-active vi T)

      (gtk-main))))