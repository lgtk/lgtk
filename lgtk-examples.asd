;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.


(defpackage #:lgtk-examples-asd
  (:use :cl :asdf))

(in-package :lgtk-examples-asd)

(defsystem lgtk-examples

  :components
  ((:module :examples
	    :components
	    ((:file "hello-world")
	     (:file "hello-world2")
	     (:file "splash-msg")
	     (:file "packing-boxes")
	     (:file "cool-button")
	     (:file "check-button")
	     (:file "radio-buttons")
	     (:file "toggle-button")
	     (:file "button-flavors")
	     (:file "tables-hw")
	     (:file "dialog")
	     (:file "entry"))))

  :depends-on (:lgtk))

