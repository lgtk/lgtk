;;;; -*- Mode: Lisp; Synatx: ANSI-Common-Lisp; Base: 10 -*-

;; (c) October 2003 by Mario S. Mommer <mommer@well-of-wyrd.net>
;;
;; This code comes under the terms of the modified BSD license ("sans
;; advertising clause"). See the file COPYING for details.

(defpackage #:lgtk-asd
  (:use :cl :asdf #+cmu :ext #+sbcl :sb-ext #+sbcl :sb-alien))

(in-package :lgtk-asd)

(defvar *ccompiler* "cc")


;; list of library/module names to  provide to pkgconfig
(defparameter *pkgconfig-modules* '("gtk+-2.0" "libglade-2.0"))


;; Split a string at whitespace.
(defun splitatspc (str)
  (labels ((whitespace-p (c)
			 (find c '(#\Space #\Tab #\Newline))))
    (let* ((posnew -1)
	   (posold 0)
	   (buf (cons nil nil))
	   (ptr buf))
      (loop while (and posnew (< posnew (length str))) do
	    (setf posold (+ 1 posnew))
	    (setf posnew (position-if #'whitespace-p str
				      :start posold))
	    (let ((item (subseq str posold posnew)))
	      (when (< 0 (length item))
		(setf (cdr ptr) (list item))
		(setf ptr (cdr ptr)))))

      (cdr buf))))

(defun pkg-config-lib-string ()
  #+sbcl (run-program "pkg-config" (list* "--libs" *pkgconfig-modules*)
		      :search t
		      :output :stream)
  #+cmu (ext:run-program "pkg-config" (list* "--libs" *pkgconfig-modules*)
			  :output :stream))

;; Get the list of libraries.
(defun get-gtk-libs-list ()
  (let ((prc (pkg-config-lib-string)))
    (if (not prc)
	(error "Could not run #\"pkg-config!")
      (let ((str (process-output prc))
	    (ecode (process-exit-code prc)))
	(if (not (eql ecode 0))
	    (error "Could not find gtk+-2.0")
	  (remove-if ;; Remove options which do not specify a lib
	   #'(lambda (x)
	       (not (equalp (subseq x 0 2) "-l"))) ;; equalp is case
						   ;; insenitive.
	   (splitatspc (read-line str))))))))


(defun pkg-config-cflags-string ()
  #+sbcl (run-program "pkg-config" (list* "--cflags" *pkgconfig-modules*)
		      :search t
		      :output :stream)
  #+cmu (ext:run-program "pkg-config" (list* "--cflags" *pkgconfig-modules*)
			  :output :stream))

(defun get-gtk-cflags ()
  (let ((prc (pkg-config-cflags-string)))
    (if (not prc)
	(error "Could not run #\"pckg-config!")
      (let ((str (process-output prc))
	    (ecode (process-exit-code prc)))
	(if (not (eql ecode 0))
	    (error "Could not find gtk+-2.0")
	  (read-line str))))))

(defparameter *gtklibs* (get-gtk-libs-list))
(defparameter *gtkcflags* (get-gtk-cflags))
(defvar *source-dir* nil)

(defclass gtk-libs-handle (c-source-file) ())

(defmethod output-files ((o operation) (c c-source-file))
  (list (make-pathname :name (component-name c)
		       :type "o"
		       :defaults (component-pathname c))))

(defmethod perform ((o compile-op) (c gtk-libs-handle))
  (unless (zerop (run-shell-command "~A ~A -c -o ~A ~A"
				    *ccompiler*
				    (namestring (component-pathname c))
				    (namestring (car (output-files o c)))
				    *gtkcflags*))
    (error 'operation-error :component c :operation o)))

(defmethod perform ((o load-op) (c gtk-libs-handle))
  (setf *source-dir* (pathname-directory (component-pathname c)))
  (load-foreign (namestring (car (output-files o c)))
		:libraries *gtklibs*))

(defsystem lgtk
  :name "lgtk"
  :version "0.0.0"
  :maintainer "Mario S. Mommer <mommer@well-of-wyrd.net>"
  :licence "BSD sans advertising clause (see file COPYING for details)"
  :description "GTK+ 2 interface"
  :long-description "A common-lisp interface for the GTK+2.0, 2.2... toolkit"

  :components
  ((:module :src
	    :components
	    ((:file "port")
	     (:file "nexus" :depends-on ("port"))
	     (:file "widgets" :depends-on ("nexus"))
	     (:file "dynaslot" :depends-on ("bindings" "nexus" "port"))
	     (:file "enums" :depends-on ("bindings"))
	     (:file "bindings" :depends-on ("port"))


	     (:gtk-libs-handle "wrappers")


	     (:file "gtkpackage" :depends-on ("widgets" "nexus"
					      "enums" "bindings"
					      "dynaslot"))

	     (:file "gtkenums" :depends-on ("gtkpackage" "enums"))
	     (:file "gtkclasshierarchy" :depends-on ("gtkpackage"))
	     (:file "gtkbindings" :depends-on ("gtkenums"
					       "gtkclasshierarchy"
					       "wrappers"))
	     (:file "gtknexus" :depends-on ("gtkbindings"))
	     (:file "gtklisp" :depends-on ("gtknexus"))

	     (:file "lgtk-utils" :depends-on ("gtklisp"))))))

;; On 2003.12.10, load-foreign was put in deathrow by the sbcl
;; developers. The following comment was made by Krystof_ and is
;; probably going to help minimize the impact: "cmucl's load-foreign
;; on linux runs ld -G -o /tmp/RANDOM --whole-archive file1.a file2.o
;; file3.so --no-whole-archive -lfoo -lbar"
